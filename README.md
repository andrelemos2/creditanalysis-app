Crédito Fácil (Análise de crédito)
==========================
Simples Projeto para análise de crédito

Última Versão
==========================
https://andrelemos2@bitbucket.org/andrelemos2/creditanalysis-app.git

Backlog
==========================
https://trello.com/b/wA5CTWvm/cr%C3%A9dito-f%C3%A1cil

Construindo na sua máquina
==========================
- git clone https://andrelemos2@bitbucket.org/andrelemos2/creditanalysis-app.git
- npm install
- ng serve --open

Técnologias utilizadas
==========================
- Angular 7 (https://angular.io)
- Angular CLI (https://cli.angular.io)
- Typescript (https://www.typescriptlang.org)
- PrimeNG (https://www.primefaces.org/primeng/#/)
