import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CreditComponent } from './components/credit-analysis/credit.component';
import { PartnerComponent } from './components/partner/partner.component';

const routes: Routes = [
  { path: '', redirectTo: '/analysis', pathMatch: 'full' },
  { path: 'analysis', component: CreditComponent },
  { path: 'partners', component: PartnerComponent }
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule { }
