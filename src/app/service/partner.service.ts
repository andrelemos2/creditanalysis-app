import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BusinessPartner } from '../domain/model';
import { environment } from '../../environments/environment';

@Injectable()
export class PartnerService {

    creditAnalysisApiUrl: string;

    constructor(private http: HttpClient) {
        this.creditAnalysisApiUrl = `${environment.apiUrl}/partners`;
    }

    findPartnerByPartialName(partialName: string): Promise<any> {
        return this.http.get<any>(`${this.creditAnalysisApiUrl}/findbyname/${partialName}`)
            .toPromise()
            .then(res => <BusinessPartner[]>res)
            .then(data => data);
    }

}