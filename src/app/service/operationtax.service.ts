import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { Tax } from '../domain/model';


@Injectable()
export class OperationTaxService {

  creditAnalysisApiUrl: string;

  constructor(private http: HttpClient) {
    this.creditAnalysisApiUrl = `${environment.apiUrl}/taxes`;
  }

  getOperationTaxes(): any {
    return this.http.get<any>(`${this.creditAnalysisApiUrl}-list`)
      .toPromise()
      .then(res => <Tax[]>res)
      .then(data => data);
  }

}