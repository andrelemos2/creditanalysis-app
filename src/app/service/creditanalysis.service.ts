import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { CreditAnalysis } from '../domain/model';
import { environment } from '../../environments/environment';
import { PartnerService } from './partner.service';

@Injectable()
export class CreditAnalysisService {

  creditAnalysisApiUrl: string;

  constructor(private http: HttpClient, private partnerService: PartnerService) {
    this.creditAnalysisApiUrl = `${environment.apiUrl}/analysis`;
  }

  getAnalysis() {
    return this.http.get<any>(`${this.creditAnalysisApiUrl}-list`)
      .toPromise()
      .then(res => <CreditAnalysis[]>res)
      .then(data => data);
  }

  addAnalysis(credit: CreditAnalysis): Promise<CreditAnalysis> {
    return this.http.post<CreditAnalysis>(`${this.creditAnalysisApiUrl}-create`, credit)
      .toPromise();
  }

  delAnalysis(credit: CreditAnalysis | number): Promise<void> {

    const id = typeof credit === 'number' ? credit : credit.id;
    const url = `${this.creditAnalysisApiUrl}/${id}`;

    return this.http.delete(url)
      .toPromise()
      .then(() => null);
  }

  update(credit: CreditAnalysis | number): Promise<CreditAnalysis> {

    const id = typeof credit === 'number' ? credit : credit.id;
    const url = `${this.creditAnalysisApiUrl}/${id}`;

    return this.http.put<CreditAnalysis>(url, credit)
      .toPromise()
      .then(response => {
        const cursoAlterado = response;

        return cursoAlterado;
      });
  }

  getPartnersWhichContains(partialName: string): Promise<any> {
    return this.partnerService.findPartnerByPartialName(partialName);
  }
}
