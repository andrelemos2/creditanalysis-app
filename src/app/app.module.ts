import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http'; 
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';

import {SidebarModule} from 'primeng/sidebar';
import {ButtonModule} from 'primeng/button';
import {TableModule} from 'primeng/table';
import {ToastModule} from 'primeng/toast';
import {DropdownModule} from 'primeng/dropdown';
import {AutoCompleteModule} from 'primeng/autocomplete';
import { DialogModule, MessageModule } from 'primeng/primeng';
import {BlockUIModule} from 'primeng/blockui';
import {FieldsetModule} from 'primeng/fieldset';
import {ConfirmDialogModule} from 'primeng/confirmdialog';
import {ConfirmationService} from 'primeng/api';
import {TooltipModule} from 'primeng/tooltip';

import { AppComponent } from './app.component';
import { CreditAnalysisService } from './service/creditanalysis.service';
import { CreditComponent } from './components/credit-analysis/credit.component';
import { PartnerComponent } from './components/partner/partner.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { MessageService } from 'primeng/components/common/messageservice';
import { OperationTaxService } from './service/operationtax.service';
import { ErrorHandlerService } from './service/error-handler.service';
import { PartnerService } from './service/partner.service';
import { AppRoutingModule } from './/app-routing.module';

@NgModule({
  declarations: [
    AppComponent,
    CreditComponent,
    PartnerComponent,
    NavbarComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    HttpClientModule,
    ReactiveFormsModule,
    AppRoutingModule,

    SidebarModule,
    ButtonModule,
    TableModule,
    DialogModule,
    ToastModule,
    DropdownModule,
    AutoCompleteModule,
    MessageModule,
    BlockUIModule,
    FieldsetModule,
    ConfirmDialogModule,
    TooltipModule
  ],
  providers: [CreditAnalysisService, OperationTaxService, PartnerService, ErrorHandlerService, MessageService, ConfirmationService],
  bootstrap: [AppComponent]
})
export class AppModule { }
