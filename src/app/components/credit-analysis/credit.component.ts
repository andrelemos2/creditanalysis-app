import { Component, OnInit } from '@angular/core';

import { CreditAnalysis, Tax, BusinessPartner } from '../../domain/model';
import { CreditAnalysisService } from '../../service/creditanalysis.service';
import { OperationTaxService } from '../../service/operationtax.service';
import { MessageService } from 'primeng/components/common/messageservice';
import { ErrorHandlerService } from '../../service/error-handler.service';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import {ConfirmationService} from 'primeng/api';

@Component({
  selector: 'app-credit',
  templateUrl: './credit.component.html',
  styleUrls: ['./credit.component.css']
})
export class CreditComponent implements OnInit {

  creditAnalysisForm: FormGroup;

  partnersSuggestion: BusinessPartner[];

  taxes: Tax[];

  selectedTax: Tax;

  displayDialog: boolean;

  credit: CreditAnalysis = {} as CreditAnalysis;

  selectedCredit: CreditAnalysis;

  newCredit: boolean;

  credits: CreditAnalysis[];

  cols: any[];

  constructor(private creditAnalysisService: CreditAnalysisService,
    private operationTaxService: OperationTaxService,
    private formBuilder: FormBuilder,
    private messageService: MessageService,
    private errorHandler: ErrorHandlerService,
    private confirmation: ConfirmationService) {
  }

  ngOnInit() {
    this.creditAnalysisForm = this.formBuilder.group({
      'businessPartner': new FormControl('', Validators.required),
    });

    this.getAnalysis();
    this.getOperationTaxes();
  }

  addAnalysis(credit: CreditAnalysis) {
    this.creditAnalysisService.addAnalysis(credit)
      .then(credit => {
        this.credits.push(credit);
        this.messageService.add({ severity: 'success', detail: 'Credit added successfully!' });
      })
      .catch(erro => this.errorHandler.handle(erro));
  }

  getAnalysis(): void {
    this.creditAnalysisService.getAnalysis().then(credits => {
      this.credits = credits;
    });
  }

  delAnalysis(credit: any) {
    this.creditAnalysisService.delAnalysis(credit)
      .then(() => {
        this.messageService.add({ severity: 'success', detail: 'Credit removed successfully!' });
      })
      .catch(error => this.errorHandler.handle(error));
  }

  update(credit: CreditAnalysis) {
    this.creditAnalysisService.update(credit)
      .then(credit => {
        this.credits[this.credits.indexOf(this.selectedCredit)] = credit;
        this.messageService.add({ severity: 'success', detail: 'Credit updated successfully!' });
      })
      .catch(error => this.errorHandler.handle(error));
  }

  getOperationTaxes(): void {
    this.operationTaxService.getOperationTaxes().then(taxes => {
      this.taxes = taxes;
    });
  }

  showDialogToAdd() {
    this.newCredit = true;
    this.credit = {} as CreditAnalysis;
    this.displayDialog = true;
  }

  save() {
    if (this.newCredit) {
      this.addAnalysis(this.credit);
    } else {
      this.update(this.credit);
    }

    this.credits = this.credits;
    this.credit = null;
    this.displayDialog = false;
  }

  delete() {
    let index = this.credits.indexOf(this.selectedCredit);
    this.credits = this.credits.filter((val, i) => i != index);
    this.delAnalysis(this.selectedCredit);
    this.credit = null;
    this.displayDialog = false;
  }

  onRowSelect(event) {
    this.newCredit = false;
    this.credit = this.cloneCredit(event.data);
    this.displayDialog = true;
  }

  cloneCredit(c: CreditAnalysis): CreditAnalysis {
    let credit = {} as CreditAnalysis;
    for (let prop in c) {
      credit[prop] = c[prop];
    }
    return credit;
  }

  searchBusinessPartner(event) {
    this.creditAnalysisService.getPartnersWhichContains(event.query).then(data => {
      this.partnersSuggestion = data;
    });
  }

  confirmRemove() {
    this.confirmation.confirm({
      message: 'Tem certeza que deseja excluir?',
      accept: () => {
        this.delete();
      }
    });
  }

}
