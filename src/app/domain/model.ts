export class CreditAnalysis {
    id: number;
    businessPartner = new BusinessPartner();
    operationTax = new Tax();
    interestAmmount: number;
    creditLimmit: number;
    creditLimmitWithTax: number;
}

export class BusinessPartner {
    id: number;
    name:string;
    description: string;
    vendor: boolean;
}

export class User {
    username: string;
    password: string;
}

export class Tax {
    name: string;
    code: string;
    tax: number;
}